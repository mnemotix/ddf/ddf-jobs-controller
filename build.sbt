import sbt.Keys.description
import Dependencies._

enablePlugins(sbtdocker.DockerPlugin, sbtassembly.AssemblyPlugin)
val meta = """META.INF(.)*""".r

lazy val root = (project in file("."))
  .settings(
    name := "ddf-jobs-controller",
    description := "manager de tâches pour le projet datapoc2",
    libraryDependencies ++= Seq(
      synaptixJobController,
      scalaTest,
      scalaLogging,
      typesafeConfig,
      jodaTime,
      logbackClassic,
      ddfannotator
    ),
    dependencyOverrides ++= Seq(
      "org.scala-lang.modules" %% "scala-xml" % "2.0.1"
    ),
    Compile / mainClass := Some("org.ddf.jobs.controller.DDFJobsController"),
    run / mainClass := Some("org.ddf.jobs.controller.DDFJobsController"),
    assembly / mainClass := Some("org.ddf.jobs.controller.DDFJobsController"),
    assembly /assemblyJarName  := "ddf-jobs-controller.jar",
    docker / imageNames := Seq(
      ImageName(
        namespace = Some("registry.gitlab.com/mnemotix/ddf"),
        repository = name.value,
        tag = Some(version.value)
      )
    ),
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    docker / buildOptions := BuildOptions(cache = false),
    docker / dockerfile := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("adoptopenjdk/openjdk11:alpine-slim")
        add(artifact, artifactTargetPath)
        run("mkdir", "-p", "/data/")
        run("mkdir", "-p", "/data/upload/")
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }

  )
