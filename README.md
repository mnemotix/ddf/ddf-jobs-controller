# ddf-jobs-controller

## LIST DE SYNAPTIX JOB

| ID                       | DESCRIPTION                                              | 
|--------------------------|----------------------------------------------------------|
| DDFMapperJob            | Extraction - mapping - chargement                        |  
 

## VARIABLES D'ENVIRONNEMENT

Liste des variables d'environnement du Controller.

Variables d'environnement Synaptix - RDF - INDEX - AMQP

| VARIABLE D'ENV.               | DESCRIPTION                            | 
|-------------------------------|----------------------------------------|
| DICTIONNAIRE_DATA_DIR         | Répertoire d'écriture des fichiers RDF |  


Les répertoires doivent être présents sur le serveur. Sur le serveur il existe déjà un répertoire ``/data/``

## LANCEMENT DES JOBS

Le job controller est un serveur AMQP. Pour l'appeler il faut un client AMQP qui supporte le RPC. Pour lancer un Job, il faut envoyer un message sur le `topic` `job.confstart`
contenant une conf en JSON dans le message AMQP. Le message devra contenir les arguments qui permettront de lancer le Job. `dicoName`, le nom du dictionnaire à traiter, et `filePath` le chemin du fichier CSV à parser.

Un exemple du Json à envoyer : 
```{
  "id" : "DDFMapperJob",
  "qname" : "org.ddf.annotator.jobs.DDFMapperJob",
  "args" : {
    "dicoName" : {
      "label" : "dicoName",
      "value" : "belgicismes"
    },
    "filePath" : {
      "label" : "filePath",
      "value" : "/chemin/du/ficher/ou/le/front/l/a/uploade/Belgicismes.csv"
    }
  }
}```

`"qname"` ne doit pas changer dans le JSON. Il permet au controller de savoir quelle classe faire tourner. 
`"id"` peut changer, et les contenus des `"value"` sont les paramêtres à changer.